<?php

namespace App\Http\Controllers;

use App\Helpers\OptionsHelper;
use App\Models\Information;
use App\Models\Match;
use App\Models\Result;
use App\Models\User;
use App\Models\Variable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $informations = Information::join('users', 'users.id', 'information.id')
            ->where('users.status', 1)
            ->orderBy('points', 'desc')
            ->get();
        $variables = Variable::find(1);
        $months = OptionsHelper::months();
        $pass1 = Hash::make("rpaulc");
        $pass2 = Hash::make("rwilfredg");
        $pass3 = Hash::make("rteddyc");
        $pass4 = Hash::make("rmartinr");
        $pass5 = Hash::make("rjavierv");
        $pass6 = Hash::make("rrogerb");
        $pass7 = Hash::make("rginob");
        $pass8 = Hash::make("rjavierg");
        $pass9 = Hash::make("rjuliow");
        $pass10 = Hash::make("rginod");
        $pass11 = Hash::make("rjorgez");
        $pass12 = Hash::make("rjuand");
        $pass13 = Hash::make("rmiguelm");
        $pass14 = Hash::make("rpablot");
        $pass15 = Hash::make("rsebastianc");
        $pass16 = Hash::make("rvictorc");
        $pass17 = Hash::make("radolfot");
        $pass18 = Hash::make("rantoniop");
        $pass19 = Hash::make("rernestoi");
        $pass20 = Hash::make("rfernandoa");
        $pass21 = Hash::make("rjaviera");
        $pass22 = Hash::make("rjorgep");
        $pass23 = Hash::make("rjuan carlosm");
        $pass24 = Hash::make("rcarlosz");
        $pass25 = Hash::make("rrobertoc");
        $pass26 = Hash::make("rgonzalon");
        $pass27 = Hash::make("rheiroll");
        $pass28 = Hash::make("rjulior");
        $pass29 = Hash::make("rrobertv");
        $pass30 = Hash::make("rrolandoj");
        $pass31 = Hash::make("rsandros");
        $pass32 = Hash::make("rvictorp");
        $pass33 = Hash::make("rgersonc");
        $pass34 = Hash::make("rjose luisa");
        $pass35 = Hash::make("remersonc");
        $pass36 = Hash::make("rmiguelm");
        $pass37 = Hash::make("rfranciscoc");
        $pass38 = Hash::make("rrobertod");
        $pass39 = Hash::make("rhansd");
        $pass40 = Hash::make("rjorgep");
        $pass41 = Hash::make("rcarlosr");
        $pass42 = Hash::make("rhectorf");
        $pass43 = Hash::make("rmarioa");
        $pass44 = Hash::make("rraulr");
        $pass45 = Hash::make("rerickc");
        $pass46 = Hash::make("rdaniele");

        Debugbar::info("paswords 1 : ".$pass1); 
        Debugbar::info("paswords 2: ".$pass2); 
        Debugbar::info("paswords 3: ".$pass3); 
        Debugbar::info("paswords 4 ".$pass4); 
        Debugbar::info("paswords: 5 ".$pass5); 
        Debugbar::info("paswords: 6 ".$pass6); 
        Debugbar::info("paswords: 7 ".$pass7); 
        Debugbar::info("paswords: 8 ".$pass8); 
        Debugbar::info("paswords: 9 ".$pass9); 
        Debugbar::info("paswords: 10 ".$pass10); 
        Debugbar::info("paswords: 11 ".$pass11); 
        Debugbar::info("paswords: 12 ".$pass12); 
        Debugbar::info("paswords: 13 ".$pass13); 
        Debugbar::info("paswords: 14 ".$pass14); 
        Debugbar::info("paswords: 15 ".$pass15); 
        Debugbar::info("paswords: 16 ".$pass16); 
        Debugbar::info("paswords: 17 ".$pass17); 
        Debugbar::info("paswords: 18 ".$pass18); 
        Debugbar::info("paswords: 19 ".$pass19); 
        Debugbar::info("paswords: 20 ".$pass20); 
        Debugbar::info("paswords: 21 ".$pass21); 
        Debugbar::info("paswords: 22 ".$pass22); 
        Debugbar::info("paswords: 23 ".$pass23); 
        Debugbar::info("paswords: 24 ".$pass24); 
        Debugbar::info("paswords: 25 ".$pass25); 
        Debugbar::info("paswords: 26 ".$pass26); 
        Debugbar::info("paswords: 27 ".$pass27); 
        Debugbar::info("paswords: 28 ".$pass28); 
        Debugbar::info("paswords: 29 ".$pass29); 
        Debugbar::info("paswords: 30 ".$pass30); 
        Debugbar::info("paswords: 31 ".$pass31); 
        Debugbar::info("paswords: 32 ".$pass32); 
        Debugbar::info("paswords: 33 ".$pass33); 
        Debugbar::info("paswords: 34 ".$pass34); 
        Debugbar::info("paswords: 35 ".$pass35); 
        Debugbar::info("paswords: 36 ".$pass36); 
        Debugbar::info("paswords: 37 ".$pass37); 
        Debugbar::info("paswords: 38 ".$pass38); 
        Debugbar::info("paswords: 39 ".$pass39); 
        Debugbar::info("paswords: 40 ".$pass40); 
        Debugbar::info("paswords: 41 ".$pass41); 
        Debugbar::info("paswords: 42 ".$pass42); 
        Debugbar::info("paswords: 43 ".$pass43); 
        Debugbar::info("paswords: 44 ".$pass44); 
        Debugbar::info("paswords: 45 ".$pass45); 
        Debugbar::info("paswords: 46 ".$pass46); 

        return view('index', compact('informations', 'variables', 'months'));
    }
    public function criterion(){
        return view('criterion');
    }
    public function championship(){
        return view('championship');
    }
    public function statistics(){
        $informations = Information::join('users', 'users.id', 'information.id')
            ->where('users.status', 1)
            ->orderBy('points', 'desc')
            ->get();
        return view('statistics', compact('informations'));
    }
    public function matches(){
        $matches = Match::where('status','>=',1)->orderBy('created_at', 'DESC')->get();
        $partyGroups = [];
        $months = OptionsHelper::months();
        foreach ($matches as $match) {
            $partyGroups["{$match->lower_limit}"][] = $match; 
        }
        Debugbar::info("matches>>>>>>> ");
        Debugbar::info($partyGroups); 
        return view('matches', compact('partyGroups', 'months'));
    }
    public function gallery(){
        return view('gallery');
    }
    public function updateRanking2(){
        $matches = Match::where('status', 1)->get();
        $pointsToIncrease = [];
        if(count($matches)){
            $pointsToIncrease = $this->createArray($matches); 
            foreach ($matches as $match) {
                $player1 = User::find($match->player1_id);
                $player2 = User::find($match->player2_id);
                if($player1->email == $match->result->winner){
                    // Player 1 winner
                    $pointsToIncrease["$player1->id"][0] += $match->result->pts_won;
                    $pointsToIncrease["$player2->id"][0] += $match->result->lost_pts;
                }else{
                    // Player 2 winner
                    $pointsToIncrease["$player2->id"][0] += $match->result->pts_won;
                    $pointsToIncrease["$player1->id"][0] += $match->result->lost_pts;
                }
                $match->upper_limit = Carbon::now()->toDateString();
                $variable = Variable::find(1);
                $variable->temporary_date = Carbon::now()->addDay()->toDateString();
                $variable->update();
                $pointsToIncrease["$player1->id"][1] ++;
                $pointsToIncrease["$player2->id"][1] ++;
                $match->status = 2;
                $match->update();
            }
            $this->updatePoints($pointsToIncrease);
        }
        return redirect('/')->with('info', 'Operación exitosa :');
    }
    public function createArray(&$matches){
        foreach($matches as $match){
            $arr["$match->player1_id"][0] = 0;
            $arr["$match->player1_id"][1] = 0;

            $arr["$match->player2_id"][0] = 0;
            $arr["$match->player2_id"][1] = 0;
        }
        return $arr;
    }
    public function calc($x, $y, $r, $pointsPrev, $pointsPrev2){
        $dp = $pointsPrev - $pointsPrev2;
        $points = $x * ($r - (1 / (pow(10, -$dp / $y) + 1)));
        return round($points);
    }
    public function updatePoints(&$pointsToIncrease){
        $variable = Variable::find(1);
        foreach ($pointsToIncrease as $id => $_) {
            $information = Information::find($id);
            $value = $pointsToIncrease[$id][0] / $pointsToIncrease[$id][1];
            $value += $variable->B3;
            $information->points += $value;
            $information->last_points_won = $value;
            $information->update();
        }
    }
    /*
    public function updatePoints(&$pointsToIncrease){
        foreach ($pointsToIncrease as $id => $_) {
            $information = Information::find($id);
            $value = $pointsToIncrease[$id][0] / $pointsToIncrease[$id][1];
            $information->points += $value;
            $information->last_points_won = $value;
            $information->update();
        }
    }*/
    public function confirmMatchScores($token, $id){
        $match = Match::find($id);
        if($match->token == $token){
            OptionsHelper::Calc($match);
            return redirect('/')->with('info', 'Operación exitosa');
        }
        return redirect('/')->with('info', '::::::::::::::::> La Operación no se pudo dar');
    }
}
