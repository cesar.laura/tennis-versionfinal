<?php

namespace App\Http\Controllers;

use App\Helpers\OptionsHelper;
use App\Mail\AppMail;
use App\Models\Match;
use App\Models\Set;
use App\Models\User;
use App\Models\Variable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp;
use Illuminate\Support\Facades\Hash;
use Mail;
use Barryvdh\Debugbar\Facade as Debugbar;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = Match::where('player1_id', Auth::user()->id)
            ->where('status', 1)->orwhere('player2_id', Auth::user()->id)
            ->where('status', 1)->orderBy('id', 'desc')->get();

        $matches_created = Match::where('player1_id', Auth::user()->id)
            ->where('status', 0)->orderBy('id', 'desc')->get();
            
        $unapproved_matches = Match::where('player2_id', Auth::user()->id)
            ->where('status', 0)->orderBy('id', 'desc')->get();
        return view('user.index',compact('matches', 'matches_created', 'unapproved_matches'));
    }
    public function matches(){
        $matches = Match::where('status',">=",1)
        ->where(function($query)
        {  
            $query ->where ('player1_id', Auth::user()->id)
             ->orwhere('player2_id', Auth::user()->id);
        } )
        ->orderBy('created_at', 'desc')->get();
        Debugbar::info("matches>>>>>>> ".Auth::user()->id);
        Debugbar::info($matches);     
        return view('user.matches', compact('matches'));
    }

   
    public function matchesCreated(){
        $matches_created = Match::where('player1_id', Auth::user()->id)
            ->where('status', 0)
            ->orderBy('id', 'desc')->get();
        return view('user.matchesCreated', compact('matches_created'));
    }
    public function unapprovedMatches(){
        $unapproved_matches = Match::where('player2_id', Auth::user()->id)
            ->where('status', 0)->orderBy('id', 'desc')->get();
        return view('user.unapprovedMatches', compact('unapproved_matches'));
    }
    public function createMatch(Request $request){  
        $users = User::where('status', 1)
            ->where('email', '<>', Auth::user()->email)
            ->orderBy('names', 'asc')->get();
        $today_date = Carbon::now()->toDateString();
        return view('user.createMatch', compact('users', 'today_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $match = new Match;
        $variable = Variable::find(1);
        $user = User::where('email', $request->email)->first();
        $match->player1_id = Auth::user()->id;
        $match->player2_id = $user->id;
        $match->date = $request->date;
        $match->lower_limit = $variable->temporary_date;
        $match->number_of_sets = $request->number_of_sets;
        $match->status = 0;
        $match->save();
        $match->token = Hash::make("{$match->date}{$match->id}");
        $match->update();
        for($i = 1; $i <= $match->number_of_sets; $i ++){
            $set = new Set;
            $set->match_id = $match->id;
            $set->number = $i;
            $set->save();
        }
        return redirect("user/$match->id/editMatch");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMatch($id)
    {   
        $match = Match::where('id', $id)->where('status', 0)->first();
        return view('user/editMatch', compact('match'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $match = Match::where('id', $id)->where('status', 0)->first();
        if($request->set_points1){
            $aux = 0;
            foreach ($match->sets as $set) {
                $set->player1 = $request->set_points1[$aux];
                $set->player2 = $request->set_points2[$aux];
                $set->update();
                $aux ++;
            }
            $data = [
                'subject'       => "Partido de Tenis ({$match->date}): Actualizado",
                'match'         => $match
            ];
            Mail::to($match->player2->email)
                ->send(new AppMail($data));
        }else{
            OptionsHelper::Calc($match);
        }
        return redirect('/user/matchesCreated')->with('info', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteMatch($id){
        $match = Match::where('id', $id)
            ->where('status', 0)
            ->where('player1_id', Auth::user()->id)
            ->first();
        $match ? $match->delete() : 0;
        return redirect('/user/matchesCreated')->with('info', 'Operación exitosa');
    }
}
