<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tennis</title>
    <style>
        *{
            font-family: Arial, Helvetica, sans-serif;
        }
        .table-container{
            border: 2px solid #00503C;
            max-width: 500px;
            background: #fff;
            color: #000;
            width: 100%;
        }
        .container-bottom p{
            text-align: center;
            background: #00503C;
            margin: 0;
            max-width:505px;
            margin-top: 16px;
        }
        .container-bottom p a{
            color: #fff;
            display: block;
            font-weight: bold;
            text-decoration: none;
        }
        .container-bottomr p a:hover{
            background: #fff;
            color: #00503C;
            outline: #fff 2px solid;
        }
        .table{
            width: 100%;
            text-align: center;
            border-spacing:  0;
        }
        .child-table{
            width: 100%;
        }
        .child-table tr th, .child-table tr td{
            width: 33%;
            border: 1px solid slategrey;
        }
        .txt-container{
            max-width: 500px;
            background: #fff;
            color: #000;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="txt-container">
        <p class="txt">Hola {{$match->player2->names}} {{$match->player2->surnames}},</p>
        <p class="txt">Se ha registrado el resultado de un partido/desafío en el que has participado. 
        </p>
        <p class="txt">Para que el partido quede registrado, por favor, confirmar el resultado  con el botón 
            “Confirmar”.
        </p>        
    </div>
    <div class="table-container">
        <table class="table">
          
            <thead class="child-table">
                <tr>
                
                    <th>#SET</th>
                    <th>{{$match->player1->names}} {{$match->player1->surnames}}</th>
                    <th>{{$match->player2->names}} {{$match->player2->surnames}}</th>
                </tr>
            </thead>
            <tbody class="child-table">
                @foreach ($match->sets as $set)
                    <tr>
                        <td>{{$set->number}}</td>
                        <td>{{$set->player1}}</td>
                        <td>{{$set->player2}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="container-bottom">
    <p><a  href="https://tenis.ranq.secel-peru.com/public/confirmMatchScores/{{$match->token}}/{{$match->id}}">Confirmar</a></p>
    </div>
    <div class="txt-container">
        <p class="txt">Saludos,</p>
        <p class="txt">Ranq-Tenis
        </p>
</html>