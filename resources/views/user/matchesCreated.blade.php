@extends('layouts.app')

@section('content')
<div class="limiter matches">
    <h1>Desafíos Pendientes</h1>

    <div class="search-boxen d-flex">

        <div class="search-result">
            @if (count($matches_created))
            @foreach ($matches_created as $match)
            <div class="benchmarks table matchesCreated">
                <ul class="table-body">
                    <li class="tr">
                        <span class="metric td" style="width: 100px;">{{$match->player1->names}}<label> {{$match->player1->surnames}}</label></span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-76"><span class="number">{{$set->player1}}</span></span>
                        @endforeach
                    </li>
                    <li class="tr">
                        <span class="metric td">{{$match->player2->names}} <label>{{$match->player2->surnames}}</label></span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-67"><span class="number">{{$set->player2}}</span></span>
                        @endforeach
                    </li>
                </ul>
                <ul class="table-footer">
                    <li class="th"></li>
                    <li class="th"><label>{{$match->date}}</label></li>
                    <li class="th"></li>
                </ul>
                <ul class="editar">
                    <a class="login101-form-btn" href="{{url("user/$match->id/editMatch")}}">EDITAR</a>
                </ul>
                <form style="margin-bottom: 20px;" action="{{ route('user.deleteMatch', $match->id) }}" method="POST" >
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger" value="Eliminar">

                </form>
            </div>
            @endforeach
            @else
            <h2>No tiene desafíos creados</h2>
            <div class="hola">
                <a href="{{ route('user.createMatch') }}" class="login101-form-btn" style="max-width: 16rem; margin:auto;">Crear Uno</a>
                </div>
             @endif
          
        </div>
        @endsection