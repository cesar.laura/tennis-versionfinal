@extends('layouts.app')

@section('content')
<div class="limiter matches">
    <h1>Partidos sin aprobar</h1>
    <form class="formulario" action="{{url("user/$match->id/editMatch")}}" method="POST">
        @csrf
        @method('PUT')
        <div class="search-boxen d-flex">
            <div class="search-result">
                    <div class="benchmarks table editMatches">
                        <ul class="table-body"> 
                            <li class="tr">
                                <span class="metric td" style="width: 60%;">{{$match->player1->names}}<label>{{$match->player1->surnames}}</label></span>
                                 <label class="clabel">
                                    @foreach ($match->sets as $set)
                                    
                                    <input  class="dateselect edit" type="number" min="0" max="10" name="set_points1[]" value="{{$set->player1}}">
                                    
                                    @endforeach   
                                 </label>       
                            </li>
                            <li class="tr">
                                <span class="metric td" style="width: 60%;">{{$match->player2->names}}<label> {{$match->player2->surnames}}</label></span>
                                <label class="clabel">   
                                    @foreach ($match->sets as $set)
                                    <input class="dateselect edit" type="number" min="0" max="10" required="required" name="set_points2[]" value="{{$set->player2}}" >
                                    <!-- <span class="td percentile-76"><span class="number">{{$set->player2}}</span></span>-->
                                     @endforeach   
                                </label>   
                            </li>
                        </ul>
                        <ul class="table-footer">
                            <li class="th"></li>
                            <li class="th"><label>{{$match->date}}</label></li>
                            <li class="th"></li>
                        </ul>
                        <ul class=" btn-footer">
                                <input type="submit" class="login101-form-btn" value="Actualizar">
                
                        </ul>               

                    </div>
                  

            </div>
            
        </div>       
    </form>
</div>
@endsection
