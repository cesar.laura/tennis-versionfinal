@extends('layouts.app')

@section('content')
<div class="limiter">
    <h1>Mis Partidos</h1>
    <div class="search-boxen d-flex">
        @if (count($matches))

        <div class="search-result">
            @foreach ($matches as $match)
            <div class="benchmarks table ">
                <ul class="table-body">
                    <li class="tr">
                        <span class="metric td" style="width: 100px;">{{$match->player1->names}}<label>{{$match->player1->surnames}}</label></span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-76"><span class="number">{{$set->player1}}</span></span>
                        @endforeach
                        {{-- points --}}
                        <span class="td percentile-76">
                            <span class="number pointsWL">
                                @if ($match->result->winner == $match->player1->email) +{{ $match->result->pts_won }}
                                @else {{ $match->result->lost_pts }}
                                @endif
                            </span>
                        </span>
                        {{-- end points --}}
                    </li>
                    <li class="tr">
                        <span class="metric td">{{$match->player2->names}} <label>{{$match->player2->surnames}}</label></span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-67"><span class="number">{{$set->player2}}</span></span>
                        @endforeach
                        {{-- points --}}
                        <span class="td percentile-76">
                            <span class="number pointsWL">
                                @if ($match->result->winner == $match->player2->email) +{{ $match->result->pts_won }}
                                @else {{ $match->result->lost_pts }}
                                @endif
                            </span>
                        </span>
                        {{-- end points --}}
                    </li>
                </ul>
                <ul class="table-footer">
                    <li class="th"></li>
                    @if ($match->type == "D")
                    <li class="th"><label>{{$match->date}}</label></li>
                    @else
                    <li class="th"><label>Campeonato</label></li>
                    @endif
                    <li class="th"></li>
                </ul>
            </div>
            @endforeach
        </div>

        @else
        <h2>No tienes partidos publicados</h2>
        @endif
    </div>
</div>
@endsection