@extends('layouts.app')

@section('content')



<div class="limiter full-matches">
<h1>Estadisticas - Al 15 Marzo</h1>

                        <div class="zui-wrapper">
                            <div class="zui-scroller">
                                <table class="zui-table">
                                    <thead class = "zui-table-head">
                                        <tr>
                                            <th class="zui-sticky-col zui-table-rad-left">Nombre</th>
                                            <th >Rnk</th>
                                            <th>Puntos</th>
                                            <th>PRT-Tot</th>
                                            <th>PRT-Gan</th>
                                            <th>PRT-Per</th>
                                            <th>PRT-%Ef</th>
                                            <th>GMS-Tot</th>
                                            <th>GMS-Gan</th>
                                            <th>GMS-Per</th>
                                            <th class="zui-table-rad-right">GMS-%Ef</th>
                                        </tr>
                                    </thead>
                                    <tbody class= "zui-table-body">
                                        <?php $aux = 1;?>
                                        @foreach ($informations as $information)
                                          <tr>
                                                <?php
                                                        $tot1 = $information->number_of_wins + $information->number_of_defeats;
                                                        $efec1 = ($tot1) ? round($information->number_of_wins * 100 / $tot1) : 100;
                                                        
                                                        $tot2 = $information->points_won_in_sets + $information->points_lost_in_sets;
                                                        $efec2 = ($tot2) ? round($information->points_won_in_sets * 100 / $tot2) : 100;
                                                ?>
                                              <td class="zui-sticky-col">{{$information->user->names}} </td>
                                              <td >{{$aux}}</td>
                                              <td >{{$information->points}}</td>
                                              <td >{{$tot1}}</td>
                                              <td >{{$information->number_of_wins}}</td>
                                              <td >{{$information->number_of_defeats}}</td>
                                              <td >{{ $efec1 }} %</td>
                                              <td >{{$tot2}}</td>
                                              <td >{{$information->points_won_in_sets}}</td>
                                              <td >{{$information->points_lost_in_sets}}</td>
                                              <td >{{ $efec2 }} %</td>                                           
                                          </tr>
                                          <?php $aux ++;?>
                                        @endforeach
                                        @foreach ($informations as $information)
                                          <tr>
                                                <?php
                                                        $tot1 = $information->number_of_wins + $information->number_of_defeats;
                                                        $efec1 = ($tot1) ? round($information->number_of_wins * 100 / $tot1) : 100;
                                                        
                                                        $tot2 = $information->points_won_in_sets + $information->points_lost_in_sets;
                                                        $efec2 = ($tot2) ? round($information->points_won_in_sets * 100 / $tot2) : 100;
                                                ?>
                                              <td class="zui-sticky-col">{{$information->user->names}} </td>
                                              <td >{{$aux}}</td>
                                              <td >{{$information->points}}</td>
                                              <td >{{$tot1}}</td>
                                              <td >{{$information->number_of_wins}}</td>
                                              <td >{{$information->number_of_defeats}}</td>
                                              <td >{{ $efec1 }} %</td>
                                              <td >{{$tot2}}</td>
                                              <td >{{$information->points_won_in_sets}}</td>
                                              <td >{{$information->points_lost_in_sets}}</td>
                                              <td >{{ $efec2 }} %</td>                                           
                                          </tr>
                                          <?php $aux ++;?>
                                        @endforeach

                                     
                                                                                                                                                                               
                                    </tbody>
                                </table>
                            </div>
                        </div>

@endsection
