@extends('layouts.app')

@section('content')
<div class="limiter">

  <div class="bottom-champion">
    <h1>COMO FUNCIONA EL RANKING</h1>
    <p>Todos los jugadores ingresan al Ranking con 200 puntos.
      Se consiguen y pierden puntos con la participación en los <em class="color_criterio">Campeonatos</em>
      y con los partidos de <em class="color_criterio">Desafío.</em></p>
      
    <p> En los <em class="color_criterio">Campeonatos</em> se ganan puntos según la ronda en la que queda
      cada jugador. Estos puntos tienen una validez de un año.</p>

    <p> ​​Los partidos de <em class="color_criterio">Desafío</em> son pactados por los jugadores según
      su conveniencia. Se debe acordar el tipo de partido de tal forma
      que permita tener un ganador (1 set, 2 sets, set de 8 games, etc).
      En estos partidos de <em class="color_criterio">Desafío</em> se ganan o pierden puntos en función a
      la cantidad de puntos en el ranking que tienen los jugadores que se enfrentan. </p>

    <p> El resultado de los partidos de <em class="color_criterio">Desafío</em> debe ser registrado por uno de los
      jugadores en la página web y aceptado por el otro jugador mediante la
      confirmación de un correo electrónico que le será enviado.
      El partido es publicado una vez que sea aceptado y será considerado
      en el Ranking correspondiente a la semana en que fue aprobado. </p>

    <p>El Ranking se actualiza todos los días domingo y considera el promedio
      de los puntos obtenidos en los partidos confirmados de <em class="color_criterio">Desafío</em> 
      de la semana (lunes a domingo). Adicionalmente, se bonifica con 2 puntos a los jugadores
      que hayan disputado al menos un partido en la semana. </p>
    <p>Los Campeonatos del Club entregan puntos de la siguiente manera: </p>
  </div>
  <div class="evidencia-criterio" align="center" style="margin-top: 15px;">

    <table class="evidencia-tabla" style="width:45%; font-size:13px; border: 1px solid black;">
      <tr class="td1">
        <th class="th1">Campeón</th>
        <th class="th1">350 puntos</th>
      </tr>
      <tr>
        <th class="th1">Finalista</th>
        <th class="th1">280 puntos</th>
      </tr>
      <tr>
        <th class="th1">Semifinal</th>
        <th class="th1">220 puntos</th>
      </tr>
      <tr>
        <th class="th1">Cuartos de Final</th>
        <th class="th1">170 puntos</th>
      </tr>
      <tr>
        <th class="th1">Ronda 16</th>
        <th class="th1">130 puntos</th>
      </tr>
      <tr>
        <th class="th1">Ronda 32</th>
        <th class="th1">90 puntos</th>
      </tr>
    </table>

  </div>

  @endsection