<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login V18</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
	<style>
		.message-size {
			font-size: 13px;
		}

		.message-margin {
			margin-left: 15px;
		}
	</style>
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
					@csrf
					<span class="login100-form-title p-b-43">
						Registrate
					</span>
					<div class="wrap-input100 validate-input" data-validate="Ingresa un correo valido: ex@abc.xyz">
						<input class="input100" type="email" name="email" value="{{ old('email') }}">
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Ingresa un solo Nombre">
						<input class="input100" type="text" name="names" value="{{ old('names') }}">
						<span class="focus-input100"></span>
						<span class="label-input100">Nombre</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Ingresa un solo Apellido">
						<input class="input100" type="text" name="surnames" value="{{ old('surnames') }}">
						<span class="focus-input100"></span>
						<span class="label-input100">Apellido</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Ingresa contraseña">
						<input class="input100" type="password" id="password" name="password" onkeyup='tam(); check();' minlength="8" size="8">
						<span class="focus-input100"></span>
						<span class="label-input100">Contraseña</span>
					</div>
					<div class="message-size message-margin" id='message1'></div>

					<div class="wrap-input100 validate-input" data-validate="Repetir contraseñas">
						<input class="input100" type="password" id="password_confirmation" name="password_confirmation" onkeyup='check();'>
						<span class="focus-input100"></span>
						<span class="label-input100">Repetir contraseña</span>
					</div>
					<div class="message-size message-margin" id='message'></div>
					<div class="container-login100-form-btn">
						<!--	
					<button class="login100-form-btn">
							Registrar
						</button>-->
						<input type="submit" class="login100-form-btn" value="Registrarse">
					</div>
				</form>
				<div class="login100-more" style="background-image: url('images/LoginM.png');">
				</div>
			</div>
		</div>
	</div>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script>
		/*
		var check = function() {
			if (document.getElementById('password').value ==
				document.getElementById('password_confirmation').value) {
				document.getElementById('message').style.color = 'green';
				document.getElementById('message').innerHTML = 'Concuerdan';
			} else {
				document.getElementById('message').style.color = 'red';
				document.getElementById('message').innerHTML = 'contraseñas no coinciden';
			}
		}*/
		var tam = function() {
			var a = document.getElementById('password').value;
			if (a.length >= 8) {
				document.getElementById('message1').style.color = 'green';
				document.getElementById('message1').innerHTML = 'correcto';
			} else {
				document.getElementById('message1').style.color = 'red';
				document.getElementById('message1').innerHTML = 'almenos 8 caracteres';
			}
		}

		var password = document.getElementById("password"),
			confirm_password = document.getElementById("password_confirmation");

		function validatePassword() {
			if (password.value != confirm_password.value) {
				confirm_password.setCustomValidity("contraseñas no coinciden");
			} else {
				confirm_password.setCustomValidity('');
			}
		}

		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;
	</script>
</body>