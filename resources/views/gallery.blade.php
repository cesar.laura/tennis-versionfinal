@extends('layouts.app')

@section('content')
<div class="limiter">
<!-- Header -->
  <div class="header-gallery">
    <h1>Galeria de Fotos</h1>
    <p></p>
  </div>

    <div class="container">
      <ul class="galeria">
        <li class="galeria__item"><img src="{{ asset('images/tennis1.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis1.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis2.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis3.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis1.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis2.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis3.jpg') }}" class="galeria__img"></li>
        <li class="galeria__item"><img src="{{ asset('images/tennis2.jpg') }}" class="galeria__img"></li>
      </ul>   
    </div>
  </div>
</div>  
@endsection