@extends('layouts.app')

@section('content')

STATISTICS

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">Jugador</th>
        <th scope="col">Rnk</th>
        <th scope="col">PTOS</th>
        <th scope="col">TOT1</th>
        <th scope="col">GAN1</th>
        <th scope="col">PER1</th>
        <th scope="col">%EFEC1</th>
        <th scope="col">TOT2</th>
        <th scope="col">GAN2</th>
        <th scope="col">PER2</th>
        <th scope="col">%EFEC2</th>
      </tr>
    </thead>
    <tbody>
        <?php $aux = 1;?>
        @foreach ($informations as $information)
            <tr>
                <?php
                    $tot1 = $information->number_of_wins + $information->number_of_defeats;
                    $efec1 = ($tot1) ? round($information->number_of_wins * 100 / $tot1) : 100;
                    
                    $tot2 = $information->points_won_in_sets + $information->points_lost_in_sets;
                    $efec2 = ($tot2) ? round($information->points_won_in_sets * 100 / $tot2) : 100;
                ?>
                <td>{{$information->user->names}} {{$information->user->surnames}}</td>
                <td>{{$aux}}</td>
                <td>{{$information->points}}</td>
                <td>{{$tot1}}</td>
                <td>{{$information->number_of_wins}}</td>
                <td>{{$information->number_of_defeats}}</td>
                <td>{{ $efec1 }} %</td>
                <td>{{$tot2}}</td>
                <td>{{$information->points_won_in_sets}}</td>
                <td>{{$information->points_lost_in_sets}}</td>
                <td>{{ $efec2 }} %</td>
            </tr>
            <?php $aux ++;?>
        @endforeach
    </tbody>
  </table>
@endsection